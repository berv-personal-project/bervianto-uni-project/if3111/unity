﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using AssemblyCSharp;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class FarmPlant : MonoBehaviour {

	[System.Serializable]
	public struct PlantData{
		public List<Creature> plant;
	}
	[System.Serializable]
	public struct PlotData{
		public List<Plot> plot;
	}
	//plant_creature_id, plant_birth_time, plant_harvest_time, plant_phase, plant_harvest_limit, plant_death, plant_till_growth, plant_id, plant_death_time

	[System.Serializable]
	public struct Plant{
		public int plant_creature_id;
		public string plant_birth_time;
		public string plant_death_time;
		public string plant_till_growth;
		public string plant_harvest_time;
		public int plant_harvest_limit;
		public int plant_phase;
		public bool plant_death;
	}

	public List<GameObject> Slot;
	public PlantData plantArray;
	public PlotData plotArray;
	public const string url1 = "http://hmlite.dev/getAllPlantDetailedInfo";
	public const string url2 = "http://hmlite.dev/getCreatureData/0";
	public const string urlrequest = "http://hmlite.dev/Plant";
	public const string urlwater = "http://hmlite.dev/WaterPlant";
	public const string urldestroy = "http://hmlite.dev/DestroyPlant";
	public const string urlharvest = "http://hmlite.dev/HarvestPlant";
	public bool update;

	// Use this for initialization
	IEnumerator Start () {
		WWW getData = new WWW(url1);
		yield return getData;
		string json = "{\"plot\":"+getData.text+"}";
		Debug.Log (json);
		bool error = false;
		try{
			plotArray = JsonUtility.FromJson<PlotData> (json);
		}catch(ArgumentException jsonEx){
			Debug.Log (jsonEx.ToString ());

			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			GameObject Player = GameObject.FindGameObjectWithTag ("Player");
			Player.GetComponent<RigidbodyFirstPersonController> ().enabled = false;
			Canvas.transform.GetChild (4).gameObject.SetActive (true);
			error = true;
		
		}
		if (error) {
			yield return new WaitForSeconds(5);
			Application.Quit ();
		}

		getData = new WWW(url2);
		yield return getData;
		json = "{\"plant\":" + getData.text + "}";
		try{
			plantArray = JsonUtility.FromJson<PlantData> (json);
		}catch(ArgumentException jsonEx){
			Debug.Log (jsonEx.ToString ());

			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			GameObject Player = GameObject.FindGameObjectWithTag ("Player");
			Player.GetComponent<RigidbodyFirstPersonController> ().enabled = false;
			Canvas.transform.GetChild (4).gameObject.SetActive (true);

			error = true;
		}
		if (error) {
			yield return new WaitForSeconds(5);
			Application.Quit ();
		}
		if (!error) {
			Spawn ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (update) {
			
		}
	}

	private void Spawn(){
		int Distance = 4;
		for (int i = 0; i < 3; i++) {
			for (int j = 1; j <= 3; j++) {
				int ID = (i * 3 + j);
				Vector3 position = new Vector3(this.transform.position.x+(float)(i*Distance), (float)this.transform.position.y, this.transform.position.z+(float)((j-1)*Distance));
				GameObject Prefabs = Resources.Load("Dirt_Pile", typeof(GameObject)) as GameObject;
				GameObject temp = Instantiate(Prefabs, position, Quaternion.identity);
				temp.name = "Slot#" + ID;
				temp.GetComponent<SlotScript> ().plot = plotArray.plot[ID-1];
				if (plotArray.plot [ID - 1].plant_id > 0) {	
					int idx = searchPlant (plotArray.plot [ID - 1].plant_creature_id);
					temp.GetComponent<SlotScript> ().harvestPhase = plantArray.plant [idx].creature_harvest_phase;
					temp.GetComponent<SlotScript> ().creatureGrowthTime = plantArray.plant [idx].creature_growth_time;
				} else {
					temp.GetComponent<SlotScript> ().harvestPhase = -1;
					temp.GetComponent<SlotScript> ().creatureGrowthTime = -1;
				}
				temp.GetComponent<SlotScript> ().enabled = true;
				temp.transform.SetParent (gameObject.transform);
				Slot.Add (temp);
			}
		}
	}

	private int searchPlant(int creature_id){
		int i = 0;
		while(i < plantArray.plant.Count && creature_id >= plantArray.plant[i].creature_id){
			if (plantArray.plant [i].creature_id == creature_id) {
				return i;
			} else {
				i++;
			}
		}

		return -1;
	}

	Plot PlanttoPlot(Plant newPlant, int plot_id, string creatureName){
		Plot newPlot = new Plot ();
		newPlot.plot_id = plot_id;
		newPlot.plant_id = -1;
		newPlot.plant_death_time = newPlant.plant_death_time;
		newPlot.plant_harvest_time = newPlant.plant_harvest_time;
		newPlot.plant_till_growth = newPlant.plant_till_growth;
		newPlot.plant_phase = newPlant.plant_phase;
		newPlot.plant_creature_id = newPlant.plant_creature_id;
		newPlot.plant_harvest_limit = newPlant.plant_harvest_limit;
		newPlot.plant_death = newPlant.plant_death;
		newPlot.creature_name = creatureName;

		return newPlot;
	}

	public void PlantingNewPlant(int creature_id, int plot_id){
		Plant newPlant;
		string jsonRequest;
		int idxCreature = searchPlant (creature_id);

		//initiate the plant variable
		if (idxCreature < 0) {
			print ("creature not found");
			return;
		} else {
			newPlant.plant_creature_id = creature_id;
			newPlant.plant_birth_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			newPlant.plant_death = false;
			newPlant.plant_death_time = DateTime.Now.AddMinutes ((double)plantArray.plant [idxCreature].creature_hunger_limit * 10).ToString("yyyy-MM-dd HH:mm:ss");
			newPlant.plant_till_growth = DateTime.Now.AddMinutes ((double)plantArray.plant [idxCreature].creature_growth_time * 10).ToString("yyyy-MM-dd HH:mm:ss");
			newPlant.plant_harvest_time = "";
			if (plantArray.plant [idxCreature].creature_name == "Toadstool" || plantArray.plant [idxCreature].creature_name == "Pumpkin") {
				newPlant.plant_harvest_limit = 1;
			} else {
				newPlant.plant_harvest_limit = 3;
			}
			newPlant.plant_phase = 1;
			jsonRequest = JsonUtility.ToJson (newPlant);
		}

		WWWForm form = PlayerStatusBehaviour.doingSomething(10);
		form.AddField ("jsonString", jsonRequest);
		form.AddField ("plot_id", plot_id);
		WWW www = new WWW (urlrequest, form);
		Plot newPlot = PlanttoPlot (newPlant, plot_id, plantArray.plant [idxCreature].creature_name); 
		StartCoroutine (WaitRequestPlant (www, newPlot, plantArray.plant[idxCreature].creature_harvest_phase, plantArray.plant[idxCreature].creature_growth_time));

	}
		

	IEnumerator WaitRequestPlant(WWW www,Plot newPlot, int harvestPhase, int growthtime)
	{
		yield return www;

		if (www.error == null) {
			int id; 
			if (Int32.TryParse (www.text, out id)) {
				Debug.Log ("Plant Id : " + id);
				newPlot.plant_id = id;
				plotArray.plot [newPlot.plot_id - 1] = newPlot;
				Slot [newPlot.plot_id - 1].GetComponent<SlotScript>().plot = plotArray.plot [newPlot.plot_id - 1];
				Slot [newPlot.plot_id - 1].GetComponent<SlotScript> ().harvestPhase = harvestPhase;
				Slot [newPlot.plot_id - 1].GetComponent<SlotScript> ().creatureGrowthTime = growthtime;
				updatePlantStatus (10);
				Slot [newPlot.plot_id - 1].GetComponent<SlotScript> ().needUpdate = true;
				update = true;
			} else {
				Debug.Log (www.text);
			}
		} else {
			Debug.Log ("Error : " + www.error);
		}
	}

	public void WateringPlot(int plot_id){
		Plot tempPlot = plotArray.plot [plot_id - 1];
		int creatureidx = searchPlant(tempPlot.plant_creature_id);
		if (creatureidx <= 0) {
			Debug.Log ("Error : Creature with id " + tempPlot.plant_creature_id  +" not found");
			return;
		}
		Creature tempPlant = plantArray.plant [creatureidx];
		string newDeathTime = Convert.ToDateTime(tempPlot.plant_death_time).AddMinutes(10*tempPlant.creature_hunger_limit*tempPlot.plant_phase).ToString("yyyy-MM-dd HH:mm:ss");

		WWWForm postRequest = PlayerStatusBehaviour.doingSomething(5);
		postRequest.AddField ("plant_id", tempPlot.plant_id);
		postRequest.AddField("death_time", newDeathTime);

		WWW Request = new WWW (urlwater, postRequest);

		StartCoroutine(WaitRequestWatering (Request, newDeathTime, plot_id));
	}

	IEnumerator WaitRequestWatering(WWW www, string newDeathTime, int plot_id){
		yield return www;
		if (www.error == null) {
			if (www.text == "Success") {
				Debug.Log ("Watering Success");
				plotArray.plot [plot_id - 1].plant_death_time = newDeathTime;
				Slot [plot_id - 1].GetComponent<SlotScript>().plot = plotArray.plot [plot_id - 1];
				updatePlantStatus (5);
				Slot [plot_id - 1].GetComponent<SlotScript>().needUpdate = true;
				update = true;
			} else {
				Debug.Log (www.text);
			}
		} else {
			Debug.Log ("Error : " + www.error);
		}
	}

	public void DestroyPlant(int plot_id){
		Plot tempPlot = plotArray.plot [plot_id - 1];

		WWWForm postRequest =PlayerStatusBehaviour.doingSomething(1);
		postRequest.AddField ("plant_id", tempPlot.plant_id);
		postRequest.AddField("plot_id", plot_id);

		WWW Request = new WWW (urldestroy, postRequest);

		StartCoroutine(WaitDestroyPlant (Request, plot_id));
	}

	IEnumerator WaitDestroyPlant(WWW www, int plot_id){
		yield return www;
		if (www.error == null) {
			if (www.text == "Success") {
				Debug.Log ("Destroying Success");
				Plot newPlot = new Plot ();
				newPlot.plot_id = plot_id;
				plotArray.plot [plot_id - 1] = newPlot;
				Slot [plot_id - 1].GetComponent<SlotScript>().plot = plotArray.plot [newPlot.plot_id - 1];
				updatePlantStatus (5);
				Slot [plot_id - 1].GetComponent<SlotScript>().needUpdate = true;
				update = true;
			} else {
				Debug.Log (www.text);
			}
		} else {
			Debug.Log ("Error : " + www.error);
		}
	}

	public void HarvestingPlot(int plot_id){
		Plot tempPlot = plotArray.plot [plot_id - 1];
		int creatureidx = searchPlant (tempPlot.plant_creature_id);
		Creature tempPlant = plantArray.plant [creatureidx];
		string newHarvestTime = Convert.ToDateTime(tempPlot.plant_harvest_time).AddMinutes(10*tempPlant.creature_growth_time*tempPlot.plant_phase).ToString("yyyy-MM-dd HH:mm:ss");
		int harvestlimit = tempPlot.plant_harvest_limit-1;
		if (harvestlimit <= 0) {
			newHarvestTime = tempPlot.plant_harvest_time;
		}
		int creature_id = tempPlot.plant_creature_id;

		WWWForm postRequest = PlayerStatusBehaviour.doingSomething(200);
		postRequest.AddField ("plant_id", tempPlot.plant_id);
		postRequest.AddField ("plant_harvest_time", newHarvestTime);
		postRequest.AddField ("plant_harvest_limit", harvestlimit);
		postRequest.AddField ("creature_id", creature_id);

		WWW Request = new WWW (urlharvest, postRequest);

		StartCoroutine(WaitHarvestPlant(Request, plot_id, newHarvestTime, creature_id));
	}

	IEnumerator WaitHarvestPlant(WWW www, int plot_id, string newHarvestTime, int creature_id){
		yield return www;
		if (www.error == null) {
			if (www.text == "Success") {
				Debug.Log ("Harvesting Success, Item Already Inside Inventory");
				//update inventory
				plotArray.plot [plot_id - 1].plant_harvest_time = newHarvestTime;
				plotArray.plot [plot_id - 1].plant_harvest_limit--;
				Slot [plot_id - 1].GetComponent<SlotScript>().plot = plotArray.plot [plot_id - 1];
				updatePlantStatus (200);
				Slot [plot_id - 1].GetComponent<SlotScript>().needUpdate = true;
				update = true;
			} else {
				Debug.Log (www.text);
			}
		} else {
			Debug.Log ("Error : " + www.error);
		}
	}

	private void updatePlantStatus(int exp){
		int newStamina = PlayerStatusBehaviour.statuscharacter.character_stamina - 10;
		int newExp = PlayerStatusBehaviour.statuscharacter.character_exp + exp;
		if (PlayerStatusBehaviour.statuscharacter.isLevelUp (newExp)) {
			PlayerStatusBehaviour.statuscharacter.character_exp = newExp - PlayerStatusBehaviour.statuscharacter.character_next_exp;
			PlayerStatusBehaviour.statuscharacter.character_stamina = PlayerStatusBehaviour.statuscharacter.character_max_stamina+10;
			PlayerStatusBehaviour.statuscharacter.character_max_stamina += 10;
			PlayerStatusBehaviour.statuscharacter.character_level += 1;
			PlayerStatusBehaviour.statuscharacter.character_next_exp *= 2;
			string value = "levelUp,"+PlayerStatusBehaviour.statuscharacter.character_level+ "," +  PlayerStatusBehaviour.statuscharacter.character_stamina;
			Arduino_Behaviour.WriteToArduino (value);
		} else {
			PlayerStatusBehaviour.statuscharacter.character_exp = newExp;
			PlayerStatusBehaviour.statuscharacter.character_stamina -= 10;
			string value = "stamina," +  PlayerStatusBehaviour.statuscharacter.character_stamina ;
			Arduino_Behaviour.WriteToArduino (value);
		}
		if (PlayerStatusBehaviour.staminaRecover != null) {
			PlayerStatusBehaviour.statuscharacter.character_stamina_recover = PlayerStatusBehaviour.staminaRecover;
		}

		StatusBehaviour.update = true;
	}
}
