﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.CompareTag ("Water")) {
			GetComponentInChildren<AudioScript> ().onWater = true; 
		}
	}

	void OnTriggerExit(Collider col){
		if (col.CompareTag ("Water")) {
			GetComponentInChildren<AudioScript> ().onWater = false; 
		}
	}
}
