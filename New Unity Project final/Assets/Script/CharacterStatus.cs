﻿using System;
using UnityEngine;
namespace AssemblyCSharp
{
	[System.Serializable]
	public class CharacterStatus
	{
		public string character_name;
		public int character_stamina;
		public int inventory_id;
		public int character_money;
		public int character_max_stamina;
		public int character_level;
		public int character_exp;
		public int character_next_exp;
		public int gift_box_id;
		public int character_id;
		public string character_stamina_recover;

		public CharacterStatus ()
		{
			character_exp = -1;
			character_name = "";
			character_id = -1;
			character_stamina = -1;
			inventory_id = -1;
			character_money = -1;
			character_max_stamina = -1;
			character_level = -1;
			character_next_exp = -1;
			gift_box_id = -1;
			character_stamina_recover = "";
		}

		public bool isLevelUp(int exp){
			return (exp) >= character_next_exp;	
		}

		public int LevelExp(int level){
			return character_next_exp * level + 20;
		}

		public bool isStaminaRecover(){
			if (character_id > 0) {
				if (character_stamina < character_max_stamina) {
					if (character_stamina_recover != "") {
						DateTime stamina_time = Convert.ToDateTime (character_stamina_recover);
						if (stamina_time.CompareTo (DateTime.Now) < 0) {
							return true;
						}
					}
				}
			} 
			return false;
		}
	}
}

