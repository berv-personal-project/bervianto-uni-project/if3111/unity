﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;
using UnityEngine.UI;
using System;

public class StatusBehaviour : MonoBehaviour {
	GameObject StaminaSlider;
	GameObject ExpSlider;
	GameObject NameAndLevel;
	GameObject Gold;
	GameObject CountDown;
	public static bool update;

	// Use this for initialization
	void Start () {
		StaminaSlider = transform.GetChild(2).gameObject;
		ExpSlider = transform.GetChild (3).gameObject;
		NameAndLevel = transform.GetChild (4).gameObject;
		Gold = transform.GetChild (5).gameObject;
		CountDown = transform.GetChild (6).gameObject;
		InstantiateUIStatus ();
		update = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (update) {
			InstantiateUIStatus ();
		}
		if (PlayerStatusBehaviour.statuscharacter != null) {
			if (PlayerStatusBehaviour.statuscharacter.character_stamina < PlayerStatusBehaviour.statuscharacter.character_max_stamina) {
				if (PlayerStatusBehaviour.statuscharacter.character_stamina_recover != "" && PlayerStatusBehaviour.statuscharacter.character_stamina_recover != null) {
					TimeSpan span = new TimeSpan (Convert.ToDateTime (PlayerStatusBehaviour.statuscharacter.character_stamina_recover).Ticks - DateTime.Now.Ticks);
					if (span.TotalSeconds > 0) {
						CountDown.GetComponent<Text> ().text = "Recovering : " + string.Format ("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds);
					} else {
						CountDown.GetComponent<Text> ().text = "";
					}
				} else {
					CountDown.GetComponent<Text> ().text = "";
				}
			} else {
				CountDown.GetComponent<Text> ().text = "";
			}
		}

	}

	void InstantiateUIStatus(){
		if (PlayerStatusBehaviour.statuscharacter != null) {
			StaminaSlider.GetComponent<Slider> ().maxValue = (float) PlayerStatusBehaviour.statuscharacter.character_max_stamina;
			StaminaSlider.GetComponent<Slider> ().minValue = 0;
			StaminaSlider.GetComponent<Slider> ().value = (float)PlayerStatusBehaviour.statuscharacter.character_stamina;
			ExpSlider.GetComponent<Slider> ().maxValue = (float) PlayerStatusBehaviour.statuscharacter.character_next_exp;
			ExpSlider.GetComponent<Slider> ().minValue = 0;
			ExpSlider.GetComponent<Slider> ().value = (float)PlayerStatusBehaviour.statuscharacter.character_exp;
			NameAndLevel.GetComponent<Text> ().text = PlayerStatusBehaviour.statuscharacter.character_name + " Lv." + PlayerStatusBehaviour.statuscharacter.character_level;
			Gold.GetComponent<Text> ().text = "Gold : "+PlayerStatusBehaviour.statuscharacter.character_money;

		}
	}
}
