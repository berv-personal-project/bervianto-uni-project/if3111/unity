﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;
using System;

public class SlotScript : MonoBehaviour {
	public GameObject newObj;
	public Plot plot;
	public int harvestPhase;
	public int creatureGrowthTime;
	public bool needUpdate;
	public bool initHarvest;
	public bool initThirsty;
	public bool receivingdata;



	public const string url1 = "http://hmlite.dev/updatePlantPhase";
	public const string url2 = "http://hmlite.dev/updatePlantDeath";
	public const string url3 = "http://hmlite.dev/updatePlantLastPhase";

	// Use this for initialization
	void Start () {		
		InstantiateObject();
		initHarvest = plot.isHarvest ();
		initThirsty = plot.isPlantThirsty ();
		receivingdata = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (needUpdate) {
			InstantiateObject ();
			needUpdate = false;
		}
		checkStatus ();
	}

	void checkStatus(){
		bool plantDeath = plot.plant_death;
		plot.checkPlantDeath ();
		if (!plantDeath && !receivingdata) {
			if (initHarvest != plot.isHarvest()) {
				needUpdate = true;
				initHarvest = plot.isHarvest ();
			}
			if (initThirsty != plot.isPlantThirsty()) {
				needUpdate = true;
				initThirsty = plot.isPlantThirsty ();
			}
			if (plot.plant_phase < harvestPhase) {
				if (plot.isPlantHaveGrown ()) {
					receivingdata = true;
					if (plot.plant_phase + 1 == harvestPhase) {
						StartCoroutine (updatePlantLastPhase ());
					} else {
						StartCoroutine (updatePlantPhase ());
					}
				}
			}
		}

		if (plot.plant_death != plantDeath && !receivingdata) {
			receivingdata = true;
			StartCoroutine (updatePlantDeath (plot.plant_death));
		}
	}

	void InstantiateObject()
	{
		Vector3 Pos = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);
		GameObject Prefabs;
		for(int i = 0; i < transform.childCount; i++){
			GameObject gameobject = gameObject.transform.GetChild (i).gameObject;
			Destroy (gameobject);
		}
		if (plot.plant_id>0) {
			if (plot.plant_death) {	
				Prefabs = Resources.Load ("FlamesEmpty", typeof(GameObject)) as GameObject; 
				newObj = Instantiate (Prefabs, Pos, Quaternion.identity);
				newObj.name = "Flame#" + plot.plot_id;
				newObj.transform.SetParent (gameObject.transform);

				
				return;
			}
			if(plot.plant_phase != 0){
				string prefabsName = plot.creature_name+"_Plant_";
				if (plot.plant_phase >= harvestPhase) {
					if (plot.isHarvest()) {
						prefabsName += "Fruit";
					} else {
						prefabsName += "Phase"+plot.plant_phase;
					}
				} else {
					prefabsName += "Phase"+plot.plant_phase;
				}
				Prefabs = Resources.Load (prefabsName, typeof(GameObject)) as GameObject; 
				newObj = Instantiate (Prefabs, Pos, Quaternion.identity);
				newObj.name = "Plant#" + plot.plot_id;
				newObj.transform.SetParent (transform);
				if (plot.isPlantThirsty()) {
					
				}
			}
		}
	}
		

	IEnumerator updatePlantPhase(){
		DateTime timeGrowth = Convert.ToDateTime (plot.plant_till_growth).AddMinutes ((double)((plot.plant_phase + 1) * creatureGrowthTime*10));
		WWWForm form = new WWWForm ();
		form.AddField ("plant_id", plot.plant_id);
		form.AddField ("plant_phase", plot.plant_phase+1);
		form.AddField ("plant_till_growth", timeGrowth.ToString ("yyyy-MM-dd HH:mm:ss"));
		WWW postRequest = new WWW (url1, form);
		yield return postRequest;
		if (postRequest.error == null) {
			Debug.Log ("Change Plant " + plot.plot_id + " Phase : " + postRequest.text);
			if (postRequest.text == "Success") {
				plot.plant_phase+=1;
				plot.plant_till_growth = timeGrowth.ToString ("yyyy-MM-dd HH:mm:ss");
				needUpdate = true;
				receivingdata = false;
			}
		} else {
			Debug.Log ("Error : " + postRequest.error);
		}
	}

	IEnumerator updatePlantLastPhase(){
		//int phasetemp = plot.plant_phase + 1;
		string timeHarvest = DateTime.Now.AddMinutes ((double)((plot.plant_phase + 1) * creatureGrowthTime*10)).ToString("yyyy-MM-dd HH:mm:ss");
		WWWForm form = new WWWForm ();
		form.AddField ("plant_id", plot.plant_id);
		form.AddField ("plant_phase", plot.plant_phase+1);
		form.AddField ("plant_harvest_time", timeHarvest);
		WWW postRequest = new WWW (url3, form);
		yield return postRequest;
		if (postRequest.error == null) {
			Debug.Log ("Change Plant " + plot.plot_id + " Phase : " + postRequest.text);
			if (postRequest.text == "Success") {
				plot.plant_phase+=1;
				plot.plant_harvest_time = timeHarvest;
				needUpdate = true;
				receivingdata = false;
			}
		} else {
			Debug.Log ("Error : " + postRequest.error);
		}
	}

	IEnumerator updatePlantDeath(bool plant_death){
		WWWForm form = new WWWForm ();
		form.AddField ("plant_id", plot.plant_id);
		form.AddField ("plant_death", plant_death.ToString());
		WWW postRequest = new WWW (url2, form);
		yield return postRequest;
		if (postRequest.error == null) {
			Debug.Log ("Change Plant " + plot.plot_id + " Death Status : " + postRequest.text);
			needUpdate = true;
			receivingdata = false;
		} else {
			Debug.Log ("Error : " + postRequest.error);
		}
	}
}
