﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;
using System;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour {

    //string dari URL ke git project di heroku

    public GameObject inventoryPanel;
    public GameObject[] inventoryIcons;
	public static string urlGetCountAll = "http://hmlite.dev/getCountAll";

    [System.Serializable]
    public struct counted
    {
        public int plant;
        public int animal;
        public int item;
    }

    // Use this for initialization
    IEnumerator Start () {
        // inisiasi database
        /*
        if (inventoryPanel == null) {
            Debug.Log("FALSE LOG");
        }*/
        
        //mendapatkan data jumlah inventory
        WWW getData = new WWW(urlGetCountAll);
        yield return getData;
        string json = getData.text;
        Debug.Log(json);
        string s = "";
        for (int i = 0; i < json.Length; i++) {
            if (json[i] != ']' && json[i] != '[') {
                s = s + json[i];
            } 
        }
        Debug.Log(s);
        //parsing json
        counted c = JsonUtility.FromJson<counted>(s);
        Debug.Log(c.plant);
        Debug.Log(c.animal);
        Debug.Log(c.item);

        //inisiasi
        int j = 0;
        foreach (Transform child in inventoryPanel.transform)
        {
            j++;
            if (j == 1)
            {
                child.Find("Text").GetComponent<Text>().text = "" + c.plant;
            }
            else if (j == 2)
            {
                child.Find("Text").GetComponent<Text>().text = "" + c.animal;
            }
            else {
                child.Find("Text").GetComponent<Text>().text = "" + c.item;
            }
        }
        
    }

    // Update is called once per frame
    void Update () {
        // 
       
    }

    void add(int index) {
        int i = 0;
        foreach (Transform child in inventoryPanel.transform)
        {
            i++;
            if (i == index) {
                string c = child.Find("Text").GetComponent<Text>().text;
                int count = System.Int32.Parse(c) + 1;
                child.Find("Text").GetComponent<Text>().text = "" + count;
            }
        }
    }

    void delete(int index) {
        int i = 0;
        foreach (Transform child in inventoryPanel.transform)
        {
            i++;
            if (i == index)
            {
                string c = child.Find("Text").GetComponent<Text>().text;
                int count = System.Int32.Parse(c) - 1;
                if (count < 0) count = 0;
                child.Find("Text").GetComponent<Text>().text = "" + count;
            }
        }
    }
}
