﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;
using System;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerStatusBehaviour : MonoBehaviour {

	const string urlstatus = "http://hmlite.dev/getCharStatus";
	const string urlStamina = "http://hmlite.dev/function/char02";
	public static CharacterStatus statuscharacter;
	public static string staminaRecover;
	public bool recovering;

	// Use this for initialization
	IEnumerator Start () {
		statuscharacter = new CharacterStatus ();
		//get Status Data
		WWWForm dataForm = new WWWForm();
		dataForm.AddField ("player_id", 1);
		WWW getData = new WWW(urlstatus, dataForm);

		yield return getData;
		string json = getData.text;
		Debug.Log (json);
		bool error = false;
		GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");

		try{
			statuscharacter = JsonUtility.FromJson<CharacterStatus> (json);
		}catch(ArgumentException jsonEx){
			Debug.Log (jsonEx.ToString ());
			GameObject Player = GameObject.FindGameObjectWithTag ("Player");
			Player.GetComponent<RigidbodyFirstPersonController> ().enabled = false;
			Canvas.transform.GetChild (4).gameObject.SetActive (true);
			error = true;
		}
		Debug.Log (json);
		if (error) {
			yield return new WaitForSeconds (5);
			Application.Quit ();
		} else {
			Canvas.GetComponent<ActivateOptionPanel> ().enabled = true;
			staminaRecover = statuscharacter.character_stamina_recover;
			string value = "characterData," + statuscharacter.character_name + "," + statuscharacter.character_level + "," + statuscharacter.character_stamina + "\n";
			Debug.Log (value);
			Arduino_Behaviour.WriteToArduino (value);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!recovering) {
			if (statuscharacter.isStaminaRecover ()) {
				TimeSpan staminaSpan = new TimeSpan (DateTime.Now.Ticks-Convert.ToDateTime (statuscharacter.character_stamina_recover).Ticks);
				if (staminaSpan.TotalSeconds > 0) {
					int totalStamina = ((int)Mathf.Floor ((float)staminaSpan.TotalMinutes) / 5)*10;
					if (totalStamina < 10) {
						totalStamina = 10;
					} else {
						print (totalStamina + " total stamina");
					}
					recovering = true;
					StartCoroutine (recoverStamina (totalStamina));
				}
			} else {
				if (statuscharacter.character_stamina < statuscharacter.character_max_stamina && (statuscharacter.character_stamina_recover == ""||statuscharacter.character_stamina_recover == null)) {
					recovering = true;
					StartCoroutine(recoverStamina(0));
				}
			}
		}
	}

	public static WWWForm doingSomething(int exp){
		int newStamina = statuscharacter.character_stamina - 10;
		int newExp = statuscharacter.character_exp + exp;
		string newStaminaRecover;
		WWWForm updateStatus = new WWWForm ();
		if (!(statuscharacter.character_max_stamina > statuscharacter.character_stamina)) {
			newStaminaRecover = DateTime.Now.AddMinutes (5).ToString ("yyyy-MM-dd HH:mm:ss");
			staminaRecover = newStaminaRecover;
		}
		updateStatus.AddField("character_stamina_recover", staminaRecover);
		updateStatus.AddField ("character_id", statuscharacter.character_id);
		if (statuscharacter.isLevelUp (newExp)) {
			updateStatus.AddField ("character_exp", newExp-statuscharacter.character_next_exp);
			updateStatus.AddField("character_level",statuscharacter.character_level+1);
			updateStatus.AddField ("character_next_exp", statuscharacter.character_next_exp*2);
			updateStatus.AddField ("character_stamina", statuscharacter.character_max_stamina+10);
			updateStatus.AddField ("character_max_stamina", statuscharacter.character_max_stamina+10);
		} else {
			updateStatus.AddField ("character_exp", newExp);
			updateStatus.AddField ("character_stamina", newStamina);
		}

		return updateStatus;
	}

	IEnumerator recoverStamina(int stamina){
		int newStamina = statuscharacter.character_stamina + stamina;
		print (newStamina + " total stamina");
		WWWForm updateStatus = new WWWForm();
		string newStaminaRecover;
		if (newStamina < statuscharacter.character_max_stamina) {
			newStaminaRecover = DateTime.Now.AddMinutes (5).ToString ("yyyy-MM-dd HH:mm:ss");
		} else {
			newStaminaRecover = statuscharacter.character_stamina_recover;
			newStamina = statuscharacter.character_max_stamina;
		}
		updateStatus.AddField ("character_stamina_recover", newStaminaRecover);
		updateStatus.AddField ("character_stamina", newStamina);

		WWW recoverStamina = new WWW (urlStamina, updateStatus);

		yield return recoverStamina;
		if (recoverStamina.error == null) {
			Debug.Log (recoverStamina.text);
			if (recoverStamina.text == "Success") {
				statuscharacter.character_stamina = newStamina;
				statuscharacter.character_stamina_recover = newStaminaRecover;
				string value = "stamina," +  PlayerStatusBehaviour.statuscharacter.character_stamina ;
				Arduino_Behaviour.WriteToArduino (value);
				StatusBehaviour.update = true;
			}
		} else {
			Debug.Log (recoverStamina.error.ToString ());
		}

		recovering = false;

	}
}
