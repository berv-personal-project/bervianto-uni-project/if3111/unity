﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantPlayerBehaviour : MonoBehaviour {

	public bool showOptionPanel;
	public bool panelStatus;
	public GameObject plant;
	public bool doingsomething;
	public bool stillplanting;

	void Start(){
		showOptionPanel = false;
		panelStatus = false;
		doingsomething= false;
	}

	// Update is called once per frame
	void Update () {
		if (showOptionPanel) {
			if (!panelStatus) {
				GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
				Canvas.GetComponent<ActivateOptionPanel> ().optionpanelon = showOptionPanel;
				panelStatus = true;
			}

		} else {
			if (panelStatus) {
				GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
				Canvas.GetComponent<ActivateOptionPanel> ().optionpanelon = showOptionPanel;
				panelStatus = false;
			}
		}
	}

	void OnCollisionStay(Collision Collision){
		if (Collision.gameObject.CompareTag ("plant")) {
			plant = Collision.gameObject;
			showOptionPanel = true;
		}
	}

	void OnCollisionExit(Collision Collision){
		if (Collision.gameObject.CompareTag ("plant")) {
			plant = null;
			showOptionPanel = false;
		}
	}

	public void PlantNewPlant(){
		if (plant != null) {
			doingsomething = true;
			plant.transform.parent.GetComponent<FarmPlant> ().PlantingNewPlant (4, plant.GetComponent<SlotScript> ().plot.plot_id);
			doingsomething = false;
		}
	}

	public void WaterThePlant(){
		if (plant != null) {
			doingsomething = true;
			plant.transform.parent.GetComponent<FarmPlant> ().WateringPlot(plant.GetComponent<SlotScript> ().plot.plot_id);
			doingsomething = false;
		}
	}

	public void HarvestThePlant(){
		if (plant != null) {
			doingsomething = true;
			plant.transform.parent.GetComponent<FarmPlant> ().HarvestingPlot(plant.GetComponent<SlotScript> ().plot.plot_id);
			doingsomething = false;
		}
	}

	public void DestroyThePlant(){
		if (plant != null) {
			doingsomething = true;
			plant.transform.parent.GetComponent<FarmPlant> ().DestroyPlant (plant.GetComponent<SlotScript> ().plot.plot_id);
			doingsomething = false;
		}
	}
}
