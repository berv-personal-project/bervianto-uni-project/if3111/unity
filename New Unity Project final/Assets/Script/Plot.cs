﻿using System;
using UnityEngine;
using AssemblyCSharp;

namespace AssemblyCSharp
{
	[System.Serializable]
	public class Plot{
		public int plot_id ;
		public int plant_id;//{ get; set; }
		public int plant_creature_id;// { get; set; }
		public string plant_death_time;// { get; set; }
		public string plant_till_growth;// { get; set; }
		public string plant_harvest_time;// { get; set; }
		public int plant_harvest_limit ;//{ get; set; }
		public int plant_phase;// { get; set; }
		public string creature_name;// { get; set; }
		public bool plant_death;// { get; set; }
		//public const double plant_thirst = 20.0;

		public Plot(){
			plot_id = -1;
			plant_id = -1;
			plant_creature_id = -1;
			plant_death_time = "";
			plant_till_growth = "";
			plant_harvest_time = "";
			plant_harvest_limit = -1;
			plant_phase = -1;
			creature_name = "";
			plant_death= false;
		}

		public void checkPlantDeath(){
			plant_death = false;
			if (plant_id > 0) {
				DateTime death_time = Convert.ToDateTime(plant_death_time);
				if (death_time.CompareTo (DateTime.Now) < 0 || plant_harvest_limit <= 0) {
					plant_death = true;
				}
			}
		}

		public bool isHarvest(){
			if (plant_id > 0) {
				if (plant_harvest_time != "") {
					DateTime harvest_time = Convert.ToDateTime (plant_harvest_time);
					if (harvest_time.CompareTo (DateTime.Now) < 0 && plant_harvest_limit > 0) {
						return true;
					}
				}
			} 
			return false;
		}

		public bool isPlantThirsty(){
			if (plant_id > 0) {
				TimeSpan timetilldeath = new TimeSpan(Convert.ToDateTime(plant_death_time).Ticks - DateTime.Now.Ticks);
				if (timetilldeath.TotalMinutes < 10.0) {
					return true;
				}
			}
			return false;
		}

		public bool isPlantHaveGrown(){
			if (plant_id > 0) {
				DateTime growth_time = Convert.ToDateTime (plant_till_growth);
				if (growth_time.CompareTo (DateTime.Now) <= 0 && plant_phase < 3) {
					return true;
				}
			} 

			return false;
		}

		public TimeSpan tillDeath(){
			TimeSpan timetilldeath = new TimeSpan(Convert.ToDateTime(plant_death_time).Ticks - DateTime.Now.Ticks);
			return timetilldeath;
		}

		public TimeSpan tillGrown(){
			TimeSpan timetillgrown = new TimeSpan(Convert.ToDateTime(plant_till_growth).Ticks - DateTime.Now.Ticks);
			return timetillgrown;
		}

		public TimeSpan tillHarvest(){
			TimeSpan timetillharvest = new TimeSpan(Convert.ToDateTime(plant_harvest_time).Ticks - DateTime.Now.Ticks);
			return timetillharvest;
		}

		public TimeSpan tillThirsty(){
			TimeSpan timetillthirst = new TimeSpan(Convert.ToDateTime(plant_death_time).AddMinutes(-10.0*plant_phase).Ticks - DateTime.Now.Ticks);
			return timetillthirst;
		}
	}
}

