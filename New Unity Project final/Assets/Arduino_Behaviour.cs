﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using AssemblyCSharp;
using System;

public class Arduino_Behaviour : MonoBehaviour {
	public static SerialPort stream = new SerialPort("COM5", 9600); //Set the port (com4) and the baud rate (9600, is standard on most devices)
	public static SerialPortLineReader reader;
	GameObject Sun;
	GameObject Player;
	//float[] lastRot = {0,0,0}; //Need the last rotation to tell how far to spin the camera
	static string value;
	public static double lightintensity;
	public AudioClip[] Song;
	public AudioSource speaker;
	int N;

	// Use this for initialization
	void Start () {
		Sun = GameObject.FindGameObjectWithTag ("Sun");
		//Song = Resources.FindObjectsOfTypeAll<AudioClip> ();
		//Debug.Log(Song.Length.ToString ());
		Player = GameObject.FindGameObjectWithTag ("Player");
		speaker = Player.GetComponentInChildren<AudioSource> ();
		N = 1;
		//stream.Close ();
		try{
			if(!stream.IsOpen){
				stream.Open ();
				reader = new SerialPortLineReader(stream);
				stream.WriteTimeout = 5000;
			}
		}catch(System.Exception){
			Debug.Log ("There is an error when opening the arduino");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (stream.IsOpen) {
			value = reader.ReadLine ();
			Debug.Log (value);
			if (value != null) {
				Debug.Log (value);
				double lightval;

				if (double.TryParse (value, out lightval)) {
					double percentage = lightval / (5f * 1333f);
					lightintensity = 8 * percentage;
					Debug.Log (lightintensity);
					Sun.GetComponent<Light> ().intensity = (float)lightintensity;
				}else{
					if(value == "ON"){
						if (speaker.isPlaying) {
							speaker.Stop ();
						}
						if (N < Song.Length) {
							speaker.clip = Song [N];
							speaker.Play ();
							N++;
						} else {
							N = 0;
							speaker.clip = Song [N];
							speaker.Play ();
						}
					}
				}

			}
		} else {
			try{
				stream.Open ();
				reader = new SerialPortLineReader(stream);
				stream.WriteTimeout = 5000;
			}catch(System.Exception){
				Debug.Log ("There is an error when opening the arduino");
			}

		}

	}

	public static void WriteToArduino(string val){
		if (stream.IsOpen) {
			try {
				stream.WriteLine (val);
			} catch (System.Exception) {
				Debug.Log ("Error in Writing data to arduino");
			}
		} else {
			Debug.Log ("Arduino not found");
		}
	}

	void OnApplicationQuit(){
		stream.Close ();
	}
		
}
