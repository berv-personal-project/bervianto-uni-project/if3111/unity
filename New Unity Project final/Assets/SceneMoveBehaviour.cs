﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class SceneMoveBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.CompareTag ("Player")) {
			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			Canvas.GetComponent<ActivateOptionPanel> ().scenechange= true;
			if (Input.GetKeyDown (KeyCode.Space)) {
				if (SceneManager.GetActiveScene ().name == "SceneFarm") {
					SceneManager.LoadScene ("Homescene");
				} else {
					SceneManager.LoadScene ("SceneFarm");
				}
			}
		}
	}

	void OnTriggerExit(Collider col){
		if (col.CompareTag ("Player")) {
			GameObject Canvas = GameObject.FindGameObjectWithTag ("PlayerCanvas");
			Canvas.GetComponent<ActivateOptionPanel> ().scenechange= false;
		}
	}
}
