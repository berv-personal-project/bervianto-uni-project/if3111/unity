﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class LightTimeScript : MonoBehaviour {

	public Material night;
	public Material morning;
	public Material dusk;
	public Material noon;
	public Material sunrise;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		DateTime time = DateTime.Now;
		float X_rotation = (float)time.Hour * 15f+ (float)time.Minute / 4f + (float)time.Second / 240f;
		gameObject.GetComponent<Transform> ().rotation.Set(X_rotation, 0f, 0f, gameObject.GetComponent<Transform>().rotation.w);
		//gameObject.GetComponent<Light> ().intensity = intensity;
//		Debug.Log (gameObject.GetComponent<Transform>().rotation.ToString());
		if (time.Hour >= 8 && time.Hour < 12) {
			RenderSettings.skybox = morning;
		} else if (time.Hour >= 12 && time.Hour < 17) {
			RenderSettings.skybox = noon;
		} else if (time.Hour >= 17 && time.Hour < 19) {
			RenderSettings.skybox = dusk;
		} else if ((time.Hour >= 19 && time.Hour < 24) || (time.Hour > 0 && time.Hour < 5)) {
			RenderSettings.skybox = night;
		} else if (time.Hour >= 5 && time.Hour < 8) {
			RenderSettings.skybox = sunrise;
		}

	}
}
